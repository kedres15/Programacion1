/*
 * 
 * Progra#1.c
 * 
 * Copyright 2017 kevin <kevin@kevin-Satellite-C45-A>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <unistd.h>

struct respuestas{
	
	char* correcta;
	char* incorrecta;
	char* pregunta;
	struct respuestas* siguiente;
};

struct tabla_hash{
	
	struct respuestas** tabla;
	int tam;
	int elementos;
};

struct nodo{
	
	int indice;
	struct nodo* izq;
	struct nodo* der;
	char* pregunta;
	
};

struct arbolS{
	 struct nodo* raiz;
	
};

struct nodo_indice{
	
	int indice;
	struct nodo_indice* siguiente;
};

struct lista_indices{
	
	struct nodo_indice* raiz;
	int cantidad;
};

/**
 * Inicializa el arbol Splay y su raiz
 * E: ninguna
 * S: el arbol creado
 * R: ninguna.
 * */
struct arbolS* crear_arbol(){
	
	struct arbolS* arbol=(struct arbolS*)calloc(1,sizeof(struct arbolS));
	arbol->raiz=(struct nodo*)calloc(1,sizeof(struct nodo));
	return arbol;
}
	
/**
 * Funcion que inicializa los nodos con las preguntas
 * y sus indices para su insercion en el arbol splay
 * E: el id de la pregunta y el texto de la pregunta
 * S: un nodo del arbol con la pregunta
 * R: ninguna
 */

struct nodo* crear_en_arbol(int id,char* preg){
	
		struct nodo* nuevo=(struct nodo*)calloc(1,sizeof(struct nodo));
		nuevo->indice=id;
		nuevo->pregunta=preg;
		nuevo->der=NULL;
		nuevo->izq=NULL;
		return nuevo;
}

/**
 * Funcion que crea de tabla de hash con 101 campos 
 * E: niguna
 * S: la tabla creada
 * R: ninguna
 */
struct tabla_hash* crear_tabla(){
	
	struct tabla_hash* tabla = calloc(1, sizeof(struct tabla_hash));
	tabla->tam = 101;
	tabla->elementos = 0;
	tabla->tabla = calloc(tabla->tam, sizeof(struct respuestas*));
	
	for (int i = 0; i < tabla->tam; ++i){
		
		tabla->tabla[i] = calloc(1, sizeof(struct respuestas));
		tabla->tabla[i]->siguiente = NULL;
	}
	return tabla;
}

/**
 * Funcion de hash
 * E: una pregunta como llava y el largo de la tabla
 * S: un indice para guardar la pregunta
 * R: Ninguna
 */
int funcion_hash(char* llave, int largo){
	
	int hashval = toascii(llave[0]);
	
	for(int j = 1; j < strlen(llave); ++j){
		
		hashval += toascii(llave[j]);
	}
	
	int resultado = (hashval % largo);
	
	return resultado;
}

/**
 * Funcion que verifica si existe una colision en el campo seleccionado
 * E: un campo de la tabla hash para comprobar si esta vacio
 * S: 0 si esta libre, 1 si no
 * R: Ninguna
 */
int coliciones(struct respuestas* respuesta){
	
	if(respuesta->siguiente == NULL && respuesta->pregunta == NULL){
		
		return 0;
	}
	else{
		
		return 1;
	}
}

/**
 * Funcion que limpia la tabla de hash y la expande si se sobrepaso el factor carga
 * E: la tabla de hash
 * S: ninguna
 * R: ninguna
 */
void limpiar_tabla(struct tabla_hash* tabla){
	
	tabla->elementos = 0;
	tabla->tam = tabla->tam*2+11;
	
	tabla->tabla = calloc(tabla->tam, sizeof(struct respuestas*));
	
	for (int i = 0; i < tabla->tam; ++i){
		
		tabla->tabla[i] = calloc(1, sizeof(struct respuestas));
		tabla->tabla[i]->siguiente = NULL;
	}
}

/**
 * Rotacion a la izquierda de un nodo padre e hijo
 * E: un nodo del arbol
 * S: un nodo que va a ser el padre del que entro
 * R: Ninguna
 * */
	
struct nodo *rotarIzq(struct nodo* hijo)
{
    struct nodo* padre = hijo->der;
    hijo->der = padre->izq;
    padre->izq = hijo;
    return padre;
}

/**
 * Rotacion a la derecha de un nodo padre e hijo
 * E: un nodo del arbol
 * S: un nodo que va a ser el padre del que entro
 * R: Ninguna
 * */
struct nodo* rotarDer(struct nodo* hijo)
{
    struct nodo* padre = hijo->izq;
    hijo->izq = padre->der;
    padre->der = hijo;
    return padre;
}

/**
 * Funcion que produce las rotaciones necesarias para llevar a la raiz el
 * nodo buscado y si no existe envia su nodo inmediato cercano(padre del nodo)
 * E: el nodo raiz y un id del nodo que se va a mover
 * S: la nueva raiz despues de chaflanear
 * R: la raiz no ser nula ni el id ser el de la raiz
 * */
struct nodo* chaflaneado(struct nodo* raiz, int id)
{
    //SI la llave esta en la raiz retorna la raiz como caso base
    if (raiz== NULL || raiz->indice == id){
        return raiz;
		}
 
    if (raiz->indice> id)
    {
        
        if (raiz->izq == NULL){
			 return raiz;
			}
        // Zig-Zig 
        if (raiz->izq->indice > id)
        {
            
            raiz->izq->izq = chaflaneado(raiz->izq->izq,id);
  
           
            raiz = rotarDer(raiz);
        }
        else if (raiz->izq->indice< id){ 
			// Zig-Zag 
          
            raiz->izq->der = chaflaneado(raiz->izq->der,id);
  
           
            if (raiz->izq->der != NULL){
                raiz->izq = rotarIzq(raiz->izq);
                }
        }
  
        
        return (raiz->izq == NULL)? raiz: rotarDer(raiz);
    }
    else 
    {
        if (raiz->der == NULL){
			 return raiz;
		 }
  
        // Zag-Zig 
        if (raiz->der->indice > id)
        {
            
            raiz->der->izq = chaflaneado(raiz->der->izq, id);
  
            // Do first rotation for root->right
            if (raiz->der->izq != NULL)
                raiz->der = rotarDer(raiz->der);
        }
        else if (raiz->der->indice < id)
        {
            // ZAG ZAG
            
            raiz->der->der = chaflaneado(raiz->der->der, id);
			raiz = rotarIzq(raiz);
        }
  
      
        return (raiz->der == NULL)? raiz: rotarIzq(raiz);
    }
}

/**
 * 
 * Funcion que elimina la ultima pregunta del camino de la trivia
 * E: la raiz del arbol y el id del nodo a borrar
 * S: el nodo raiz nuevo
 * R: la raiz no ser nula
 * */
struct nodo* borrarNodo(struct nodo* raiz, int id)
{
    struct nodo* tmp;
    if (!raiz){
        return NULL;
     }
    // Hace splay a llave porque es lo que se busca de manera inmediata  
    raiz = chaflaneado(raiz,id);
     
    // Si la llave ya no existia entonces solo retorna la raiz 
    if (id != raiz->indice)
        return raiz;
         
    // Si la llave existe y no hay subarbol izquierdo
    // entonces recorre el subarbol derecho  
    if (!raiz->izq)
    {
        tmp = raiz;
        raiz = raiz->der;
    }
     
    // Si existe subarbol izquierdo
    else
    {
        tmp = raiz;
 
       
        raiz = chaflaneado(raiz->izq, id);
         
        
        raiz->der = tmp->der;
    }
     
 
    free(tmp);
     
 
    return raiz;
     
}

/**
 * 
 * Funcion que inserta nuevas preguntas al arbol Splay 
 * E: el nodo a insertar el id y el texto de la pregunta
 * S: La nueva raiz
 * R: que nada sea nulo
 * */

struct nodo* insertar(struct nodo* n_nodo,int indice,char* pregunta){
	
		if(n_nodo==NULL){
			
			return crear_en_arbol(indice,pregunta);
			
			}else{
				
				if (n_nodo->indice >indice){
					
							n_nodo->izq=insertar(n_nodo->izq,indice,pregunta);
							return n_nodo;
						
						}else{
							
							n_nodo->der=insertar(n_nodo->der,indice,pregunta);
							return n_nodo; 
							
						}
				
				}
			
	

	
}

/**
 * Verifica que el indice aleatorio seleccionado no este en la tabla
 * E: el indice que salio leatorio, la lista con los indices de todas las preguntas hasta ahora
 * S: 0 si no esta, 1 si si esta
 * R: Ninguna
 */
int verificar_indice(int valor, struct lista_indices* indices){
	
	if (indices->raiz == NULL){
		
		return 0;
	}
	else {
		
		int bandera = 0;
		struct nodo_indice* actual = indices->raiz;
		
		while(actual != NULL && bandera != 1){
			
			if (actual->indice == valor){
				
				bandera = 1;
			}
			actual = actual->siguiente;
		}
		return bandera;
	}
}

/**
 * Inseta el indice aleatorio a la lista que lleva el control
 * E: valor del indice nuevo que salio, la lista con todos los indices
 * S: ninguna
 * R: ninguna
 */
void insertar_indice(int valor, struct lista_indices* indices){
	
	if (indices->raiz == NULL){
		
		indices->raiz = calloc(1, sizeof(struct nodo_indice));
		indices->raiz->indice = valor;
		indices->raiz->siguiente = NULL;
	}
	else{
		
		struct nodo_indice* actual = indices->raiz;
		
		while(actual->siguiente != NULL){
			
			actual = actual->siguiente;
		}
		actual->siguiente = calloc(1, sizeof(struct nodo_indice));
		actual->siguiente->indice = valor;
		actual->siguiente->siguiente = NULL;
	}
}

/**
 * Funcion que Separa la linea leia por los campos necesarios y los guarda
 * en el arbol binario usando la Variable 'corte'
 * E: la linea leida del archivo y el arbol binario completo
 * S: ninguna
 * R: ninguna
 */
void cargar_arbol(char* linea, struct arbolS* arbolM){
	
	char* corte = NULL;///Variable que guarda el valor despues de hacer cada corte
	
	corte = strtok(linea, "|");///Aqui corte tiene el valor para el indice del arbol
	
	int id=atoi(corte);
	
	corte = strtok(NULL, "|");
	
	arbolM->raiz=insertar(arbolM->raiz,id,corte);
	arbolM->raiz=chaflaneado(arbolM->raiz,id);
	
}

/**
 * Funcion que resibe la tabla de hash y una linea del documento y guarda los datos en la tabla de hash
 * E: linea del documento, tabla hash
 * S: 0 si guardo correctamente, 1 si el factor carga fue excedido
 * R: los datos guardados tienen que ocupar menos de la mitad de la tabla
 */
int cargar_tabla(char* linea, struct tabla_hash* tabla){
	
	char* corte = NULL;///Variable que guarda el valor despues de hacer cada corte
	int indhash = 0, colicion = -1;
	struct respuestas* respuesta = NULL;
	tabla->elementos += 1;
	
	int factor_carga = tabla->tam/2;
	
	if(factor_carga <= tabla->elementos){
		
		return 1;
	}
	
	corte = strtok(linea, "|");///Aqui corte tiene el valor para el indice del arbol
	
	
	for (int i = 1; i < 4; ++i){
		
		corte = strtok(NULL, "|");
		if (i == 1){
			
			indhash = funcion_hash(corte, tabla->tam);
			respuesta = tabla->tabla[indhash];
			colicion = coliciones(respuesta);
			while (colicion != 0){
				
				if (respuesta->siguiente == NULL){
					
					respuesta->siguiente = calloc(1, sizeof(struct respuestas));
				}
				respuesta = respuesta->siguiente;
				colicion =coliciones(respuesta);
				
			}
			
			
			respuesta->pregunta = corte;//Aqui corte tiene la pregunta
		}
		else if (i == 2){
			
			respuesta->correcta = corte;//aqui corte tiene la respuesta correcta
			
		}
		else if(i == 3){
			
			corte = strtok(corte, "\n");
			respuesta->incorrecta = corte;//aqui corte tiene la respuesta incorrecta
		}
		
	}
	
	return 0;
}

/**
 * Esta funcion hace que el usuario puede hacer el ingreso de la opcion que quiere elegir
 * y valida si fue correcta.
 * E: un numero aleatorio que si es 1 pone la respueta correcta en la A y si no en la B
 * S: 0 si adivino la pregunta 1 si no
 * R: Ninguna
 */
int validar_respuesta(int num){
	
	char e = ' ';
	
	printf("\n¿Su eleccion es?: ");
	scanf(" %c", &e);
	
	if (num==1){
		if(e == 'a' || e == 'A'){
			
			printf("Respuesta correcta \n\n");
			sleep(1);
			system("clear");
			return 0;
		}
		else{
		
			printf("Respuesta incorrecta \n\n");
			sleep(1);
			system("clear");
			return 1;
		} 
	}else{
		if(e == 'b' || e == 'B'){
		
		printf("Respuesta correcta \n\n");
		sleep(1);
		system("clear");
		return 0;
		}
		else{
		
			printf("Respuesta incorrecta \n\n");
			sleep(1);
			system("clear");
			return 1;
		
			}
	}
}

/**
 * Funcion que busca las respuestas de la pregunta seleccionada
 * E: la llave para la tabla y la tabla de hash
 * S: la respuestas que se consiguieron con la llave
 * R: ninguna
 */
struct respuestas* buscar_respuestas(char* llave, struct tabla_hash* tabla){
	
	int indice = funcion_hash(llave, tabla->tam);
	struct respuestas* respuesta = tabla->tabla[indice];
	
	while(strcmp(respuesta->pregunta, llave) != 0 && respuesta != NULL){
		
		respuesta = respuesta->siguiente;
	}
	
	return respuesta;
}


/**
 * 
 * Funcion que busca las preguntas en el arbol y la vez buscara las respuestas, ademas de 
 * dar tiempo al jugador de responder
 * E: la raiz del arbol, el id de la pregunta a buscar y la tabla hash
 * S: 0 si gano la ronda mientras se llegaba a la pregunta que se busco, 1 si no
 * R: el arbol no puede ser nulo
 * 
 * */
int buscarPreguntas(struct nodo* raiz,int generado, struct tabla_hash* tabla){
	
	srand(time(NULL));
	int indice = 0, eleccion = 0;
	struct respuestas* respuesta = NULL;
	
	if (raiz==NULL){
		
		return -1;
		}
		
	struct nodo* actual=raiz;
	int num;
	
	while(actual!=NULL){
		
		if (eleccion == 0){//Esa condicion hace que no siga mostrando preguntas si respondio mal en esa ronda
			
			num=rand()% 3 + 1 ; 
			printf("%s\n",actual->pregunta);
			respuesta = buscar_respuestas(actual->pregunta, tabla);
			
			if (num==1){
				
				printf("A:%s\n", respuesta->correcta);
				printf("B:%s\n", respuesta->incorrecta);
				eleccion = validar_respuesta(num);
			}else{
				
				printf("A:%s\n", respuesta->incorrecta);
				printf("B:%s\n", respuesta->correcta);
				eleccion = validar_respuesta(num);	
			}
		}
		
		if(actual->indice==generado){
				
			return eleccion; //Este return eleccion le indica a la funcio si gano o perdio la ronda
		}else{
			
			if (actual->indice < generado){
				
				actual=actual->der;
				
			}else{
				
				actual=actual->izq;
			}
		}
	}
	
	return eleccion;
}

/**
 * Funcion que Abre el archivo y lee sus lineas
 * E: la tabla de hash vacia y el arbol binario vacio.
 * S: ninguna
 * R: ninguna
 */
void leer(struct tabla_hash* tabla, struct arbolS* arbolM){
	
	FILE* fp = NULL;
	fp = fopen("TriviaVideojuegos.txt","r");
	char* caracteres = calloc(150, sizeof(char));
	
	while((fgets(caracteres, 150, fp)) != 0){
	
		char* linea = calloc(strlen(caracteres)+1, sizeof(char));
		strcpy(linea, caracteres);
		strcat(linea, "\0");
		cargar_arbol(linea, arbolM);
	}
	
	rewind(fp);
	int cargo = 1;
	
	while((fgets(caracteres, 150, fp)) != 0){
	
		char* linea = calloc(strlen(caracteres)+1, sizeof(char));
		strcpy(linea, caracteres);
		strcat(linea, "\0");
		cargo = cargar_tabla(linea, tabla);
		
		if (cargo == 1){
			
			rewind(fp);
			limpiar_tabla(tabla);
		}
	}
	
	fclose(fp);
}

///Funcion que muestra un pequeño menu de inicio
void menu(){
	
	printf("============================================================\n");
	printf("||                     MEISLOAP                           ||\n");
	printf("|| La trivia mas enreversadamente infernal que se le pudo ||\n");
	printf("||                 ocurrir al profe...                    ||\n");
	printf("||                                                        ||\n");
	printf("||                Basado en videojuegos                   ||\n");
	printf("||                                                        ||\n");
	printf("============================================================\n");
	printf("\n");
	printf(" 	Presione Enter para comenzar :D\n");
	char inicio='e';
	scanf("%c",&inicio);
	
	}

/**
 * Funcion principal que hace correr el juego de trivias
 * Sin entradas, salidas o restricciones.
 * 
 */
void juego(){
	
	struct tabla_hash* hash = crear_tabla();
	struct arbolS* arbolM = crear_arbol();
	struct lista_indices* indices = calloc(1, sizeof(struct lista_indices));
	indices->raiz = NULL;
	indices->cantidad = 0;
	int gano = -1, indice = -1,ganes = 0, indice_valido;//gano representa si gano o perdio la ronda actual, y ganes cuantos lleva en total
	srand(time(NULL));
	char eleccion = 's';
	
	leer(hash, arbolM);
	
	while(eleccion == 's' || eleccion == 'S'){
		
		system("clear");
		indice_valido = -1;
		
		while(indice_valido != 0){//esta parte verifica que no repita los indices
			
			indice = rand()%hash->elementos;
			
			indice_valido = verificar_indice(indice, indices);
		}
		
		indices->cantidad += 1;
		insertar_indice(indice, indices);
		
		gano = buscarPreguntas(arbolM->raiz,indice, hash);
		arbolM->raiz=borrarNodo(arbolM->raiz,indice);
		arbolM->raiz=chaflaneado(arbolM->raiz,indice);
		
		if (gano == 0){
			
			printf("Felicidades usted gano la ronda\n");
			++ganes;
		}
		else{
			
			printf("Perdio la ronda\n");
		}
		
		if (indices->cantidad < hash->elementos){
			printf("Desea jugar otra ronda?(S/N)");
			scanf(" %c", &eleccion);
			printf("\n\n");
		}
		else{
			
			eleccion = 'n';
		}
	}
	
	printf("Usted gano %i ronda(s).\n", ganes);
	
	free(hash);
	hash = NULL;
	
	free(arbolM);
	arbolM = NULL;
	
	free(indices);
	indices = NULL;
}

///Funcion main del programa
int main(int argc, char **argv)
{
	menu();
	
	juego();
	
	return 0;
}

